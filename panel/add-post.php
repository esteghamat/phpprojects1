<?php include_once "..\init.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Add-Post</title>
    <style>
        body * {
        display: block;
        margin: 10px auto;
        }
    </style>
</head>
<body>
    <form action="..\panel\process\save-post.php" method="post">
        <input type="text" name="title" placeholder="enter a title">
        <select name="author">
            <option value="Ali">Ali</option>
            <option value="Mohammad">Mohammad</option>
            <option value="Zinat">Zinat</option>
            <option value="Saeed">Saeed</option>
            <option value="Canan">Canan</option>
        </select>
        <textarea name="content" cols="30" rows="10"></textarea>
        <button type="submit">Save</button>
    </form>
</body>
</html>