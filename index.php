<?php 
include_once "init.php"; 
$posts = getpost(0);
//var_dump($posts);
//echo gettype($posts);
//var_dump(siteUrl('panel'));
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>MiniCMS</title>
</head>
<body>
    <?php if(isAdmin()): ?>
            <a href="<?= siteUrl('panel') ?>">Administration Panel</a>
    <?php endif ?>
    <?php foreach($posts as $pid=>$post): ?>
        <div class="post-box">
            <span class="author"><?= $post->author ?></span>
            <h2><?= $post->title ?></h2>
            <div class="content">
            <?= $post->content ?>
            </div>
        </div>
    <?php endforeach?>
</body>
</html>