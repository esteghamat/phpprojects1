<?php

function getpost($return_assoc){
    $posts = json_decode(file_get_contents(POST_DB),$return_assoc);
    return $posts;
}

function savepost(object $post):bool
{
    $posts = getpost(1);
    $posts[]=(array)$post;
    $posts_json = json_encode($posts);
    file_put_contents(POST_DB,$posts_json);
    return true;

}

function isAdmin($user_name=0)
{
    return true;
}

function siteUrl($siteuri = '')
{
    return "http://php3.exp/minicms/$siteuri";
}